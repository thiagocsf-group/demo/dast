from unittest import TestCase
from unittest.mock import patch

from src.initialize import initialize_dast_logs
from .mock_config import ToConfig


class TestInitialize(TestCase):

    def test_initialize_dast_logs_sets_log_level_info(self):
        config = ToConfig(zap_debug=False)

        with patch('src.initialize.logging') as mock_logging:
            initialize_dast_logs(config)

        mock_logging.basicConfig.assert_called_once_with(
            level=mock_logging.INFO,
            format='%(asctime)s %(message)s',
        )

    def test_initialize_dast_logs_sets_log_level_debug_if_config_debug_set(self):
        config = ToConfig(zap_debug=True)

        with patch('src.initialize.logging') as mock_logging:
            initialize_dast_logs(config)

        mock_logging.basicConfig.assert_called_once_with(
            level=mock_logging.DEBUG,
            format='%(asctime)s %(message)s',
        )
