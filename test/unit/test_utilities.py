from unittest import TestCase

from src import utilities


class TestUtilities(TestCase):

    def test_is_url_returns_true_if_given_url(self):
        self.assertTrue(utilities.is_url('https://url.com'))

    def test_is_url_returns_false_if_not_given_url(self):
        self.assertFalse(utilities.is_url('not_url'))

    def test_is_url_returns_true_for_url_without_top_level_domain(self):
        self.assertTrue(utilities.is_url('http://test_url'))
