import unittest
from datetime import datetime
from unittest.mock import MagicMock

from src.models.http import HttpMessages
from src.report import SecurityReportFormatter
from test.unit.factories.models import f_alert
from test.unit.factories.report import vulnerability as f_vulnerability


class TestSecurityReportFormatter(unittest.TestCase):

    def setUp(self):
        self.scanned_resources = HttpMessages()
        self.zap_version = 'D-2019-09-23'
        alerts = [f_alert()]
        self.config = MagicMock()
        self.end_date_time = datetime(2020, 5, 21, 20, 46, 43, 50218)
        self.scan_report_formatter = MagicMock()
        self.scan_report_formatter.format_report = MagicMock(return_value={
            'scanned_resources': [
                {'url': 'https://test.com'},
            ],
        })

        self.vulnerability_report_formatter = MagicMock(format=MagicMock(return_value=f_vulnerability()))

        self.formatter = SecurityReportFormatter(
            self.scan_report_formatter, self.vulnerability_report_formatter, self.config)
        self.report = self.formatter.format_report(self.zap_version, alerts, self.scanned_resources, self.end_date_time)

    def test_should_contain_a_version(self):
        self.assertTrue(self.report['version'])

    def test_scanned_resources_included(self):
        self.assertEqual(len(self.report['scan']['scanned_resources']), 1)

    def test_vulnerabilities_are_sorted_by_risk(self):
        alert_a = f_alert(name='alert.a', risk='Informational')
        alert_b = f_alert(name='alert.b', risk='Low')
        alert_c = f_alert(name='alert.c', risk='Medium')
        alert_d = f_alert(name='alert.d', risk='High')
        alerts = [alert_a, alert_c, alert_b, alert_d]

        vulnerability_report_formatter = MagicMock(format=MagicMock(return_value=f_vulnerability()))
        formatter = SecurityReportFormatter(self.scan_report_formatter, vulnerability_report_formatter, self.config)
        formatter.format_report(self.zap_version, alerts, self.scanned_resources, self.end_date_time)

        self.assertEqual(len(vulnerability_report_formatter.mock_calls), 4)
        self.assertEqual(vulnerability_report_formatter.mock_calls[0][1][0].name, 'alert.d')
        self.assertEqual(vulnerability_report_formatter.mock_calls[1][1][0].name, 'alert.c')
        self.assertEqual(vulnerability_report_formatter.mock_calls[2][1][0].name, 'alert.b')
        self.assertEqual(vulnerability_report_formatter.mock_calls[3][1][0].name, 'alert.a')

    def test_only_noisy_vulnerabilities_are_aggregated(self):
        alert_a = f_alert(name='alert.noisy1', risk='High', plugin_id='10036')
        alert_b = f_alert(name='alert.noisy2', risk='High', plugin_id='10036')
        alert_c = f_alert(name='alert.b', risk='High')
        alert_d = f_alert(name='alert.c', risk='High')
        alerts = [alert_a, alert_c, alert_b, alert_d]

        self.config.aggregate_vulnerabilities = True
        self.config.browserker_scan = False

        vulnerability_report_formatter = MagicMock(format=MagicMock(return_value=f_vulnerability()))
        formatter = SecurityReportFormatter(self.scan_report_formatter, vulnerability_report_formatter, self.config)
        formatter.format_report(self.zap_version, alerts, self.scanned_resources, self.end_date_time)

        self.assertEqual(len(vulnerability_report_formatter.mock_calls), 3)
        self.assertEqual(vulnerability_report_formatter.mock_calls[0][1][0].name, 'alert.b')
        self.assertEqual(vulnerability_report_formatter.mock_calls[0][1][0].is_aggregated, False)
        self.assertEqual(vulnerability_report_formatter.mock_calls[1][1][0].name, 'alert.c')
        self.assertEqual(vulnerability_report_formatter.mock_calls[1][1][0].is_aggregated, False)
        self.assertEqual(vulnerability_report_formatter.mock_calls[2][1][0].name, 'alert.noisy1')
        self.assertEqual(vulnerability_report_formatter.mock_calls[2][1][0].is_aggregated, True)

    def test_aggregate_vulnerabilities_for_browserker_scans(self):
        alert_a = f_alert(name='alert.noisy1', risk='High', plugin_id='10036')
        alert_b = f_alert(name='alert.noisy2', risk='High', plugin_id='10036')
        alert_c = f_alert(name='alert.b', risk='High')
        alert_d = f_alert(name='alert.c', risk='High')
        alerts = [alert_a, alert_c, alert_b, alert_d]

        self.config.aggregate_vulnerabilities = False
        self.config.browserker_scan = True

        vulnerability_report_formatter = MagicMock(format=MagicMock(return_value=f_vulnerability()))
        formatter = SecurityReportFormatter(self.scan_report_formatter, vulnerability_report_formatter, self.config)
        formatter.format_report(self.zap_version, alerts, self.scanned_resources, self.end_date_time)

        self.assertEqual(len(vulnerability_report_formatter.mock_calls), 3)
        self.assertEqual(vulnerability_report_formatter.mock_calls[2][1][0].is_aggregated, True)

    def test_only_aggregate_vulnerabilities_when_aggregate_vulnerabilities_set_or_on_browserker_scans(self):
        alert_a = f_alert(name='alert.noisy', risk='High', plugin_id='10036')
        alert_b = f_alert(name='alert.noisy', risk='High', plugin_id='10036')
        alert_c = f_alert(name='alert.b', risk='High')
        alerts = [alert_a, alert_c, alert_b]

        self.config.aggregate_vulnerabilities = False
        self.config.browserker_scan = False

        vulnerability_report_formatter = MagicMock(format=MagicMock(return_value=f_vulnerability()))
        formatter = SecurityReportFormatter(self.scan_report_formatter, vulnerability_report_formatter, self.config)
        formatter.format_report(self.zap_version, alerts, self.scanned_resources, self.end_date_time)

        self.assertEqual(len(vulnerability_report_formatter.mock_calls), 3)
        self.assertEqual(vulnerability_report_formatter.mock_calls[0][1][0].name, 'alert.b')
        self.assertEqual(vulnerability_report_formatter.mock_calls[1][1][0].name, 'alert.noisy')
        self.assertEqual(vulnerability_report_formatter.mock_calls[2][1][0].name, 'alert.noisy')
