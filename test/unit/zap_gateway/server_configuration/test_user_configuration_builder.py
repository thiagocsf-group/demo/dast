from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.server_configuration import UserConfigurationBuilder


class TestUserConfigurationBuilder(TestCase):

    def test_should_add_user_configuration(self):
        config = MagicMock(zap_other_options='-config database.compact=true -config database.newsession=true')

        configuration = UserConfigurationBuilder(config).build()

        self.assertEqual(4, len(configuration))
        self.assertIn('-config database.compact=true', ' '.join(configuration))
        self.assertIn('-config database.newsession=true', ' '.join(configuration))

    def test_does_not_add_user_configuration_when_there_is_none(self):
        config = MagicMock(zap_other_options=None)

        configuration = UserConfigurationBuilder(config).build()

        self.assertEqual([], configuration)
