from datetime import datetime
from unittest import TestCase
from unittest.mock import DEFAULT, Mock, patch

from src.models import Target
from src.zap_gateway.zap_database import HttpMessagesQuery


class TestHttpMessagesQuery(TestCase):

    def test_select_fetches_messages(self):
        results = [
            (
                '1',
                'GET',
                '\r\n'.join([
                    'GET http://host.docker.internal:8010/ HTTP/1.1',
                    'User-Agent: python-requests/2.20.1',
                ]),
                '\r\n'.join([
                    'HTTP/1.1 200 OK',
                    'Server: nginx/1.17.6',
                    '',
                ]),
                '1604102339123',
                'http://host.docker.internal:8010/',
            ),
        ]
        database = Mock()
        database.select_many.return_value = results
        query = HttpMessagesQuery(database, Target('http://host.docker.internal:8010'))

        with patch.multiple(
            'src.zap_gateway.zap_database.http_messages_query',
            HttpHeadersParser=DEFAULT,
            HttpStatusParser=DEFAULT,
        ) as mock_deps:
            mock_headers_parser = mock_deps['HttpHeadersParser']
            mock_status_parser = mock_deps['HttpStatusParser']
            mock_headers_parser.return_value.parse.return_value = 'http.headers'
            mock_status_parser.return_value.parse.return_value = (200, 'OK')

            messages = query.select('histtype', ['type1', 'type2'])

        database.select_many.assert_called_once_with(
            'SELECT historyid,method,reqheader,resheader,timesentmillis,uri '
            'FROM history where histtype IN (type1,type2)',
        )
        self.assertEqual(1, len(messages))
        self.assertEqual('1', str(messages[0].message_id))
        self.assertEqual('GET', messages[0].request.method)
        self.assertEqual('http://host.docker.internal:8010/', messages[0].request.url)
        self.assertEqual('http.headers', messages[0].request.headers)
        self.assertEqual(200, messages[0].response.status)
        self.assertEqual('OK', messages[0].response.reason_phrase)
        self.assertEqual('http.headers', messages[0].response.headers)
        self.assertEqual(messages[0].time_sent, datetime(2020, 10, 30, 23, 58, 59, 123000))

    def test_should_not_return_messages_if_there_are_none(self):
        database = Mock()
        database.select_many.return_value = []
        query = HttpMessagesQuery(database, Target('http://website.test'))

        messages = query.select('field', ['value'])

        self.assertEqual(0, len(messages))

    def test_should_not_return_external_resources(self):
        results = [
            (
                '1',
                'GET',
                '\r\n'.join([
                    'GET http://cdn.test/jquery.js HTTP/1.1',
                    'User-Agent: python-requests/2.20.1',
                ]),
                '\r\n'.join([
                    'HTTP/1.1 200 OK',
                    'Server: nginx/1.17.6',
                    '',
                ]),
                '1604102339123',
                'http://cdn.test/jquery.js',
            ),
            (
                '1',
                'GET',
                '\r\n'.join([
                    'GET http://cdn.test/bootstrap.css HTTP/1.1',
                    'User-Agent: python-requests/2.20.1',
                ]),
                '\r\n'.join([
                    'HTTP/1.1 200 OK',
                    'Server: nginx/1.17.6',
                    '',
                ]),
                '1604102339123',
                'http://cdn.test/bootstrap.css',
            ),
        ]
        database = Mock()
        database.select_many.return_value = results

        query = HttpMessagesQuery(database, Target('http://website.test'))

        messages = query.select('field', ['value'])

        self.assertEqual(0, len(messages))
