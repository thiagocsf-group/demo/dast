from subprocess import PIPE, Popen
from unittest import TestCase
from unittest.mock import MagicMock, Mock

from src.zap_gateway.zap_server_daemon import ZAPServerDaemon
from test.unit import utilities


class TestZAPServerDaemon(TestCase):

    def test_proxy_endpoint_returns_url(self):
        daemon = ZAPServerDaemon(666, Mock())

        self.assertEqual(daemon.proxy_endpoint(), 'http://127.0.0.1:666')

    def test_should_raise_error_when_shutting_down_and_zap_has_already_stopped(self):
        process = Popen('echo', stdout=PIPE, stderr=PIPE)

        # wait for the process to finish
        process.communicate()

        with self.assertRaises(RuntimeError) as error:
            ZAPServerDaemon(666, process).shutdown()

        self.assertIn('Failed to shutdown ZAP as ZAP has already exited', str(error.exception))
        self.assertIn('This indicates an error occurred with the scan', str(error.exception))

    def test_should_shutdown_the_zap_daemon(self):
        process = MagicMock()
        process.poll.return_value = None

        with utilities.httpserver.new().respond(status=200, content='{"Result":"OK"}').build() as server:
            ZAPServerDaemon(server.port(), process).shutdown()

        self.assertEqual(1, len(server.handled_requests()))
        self.assertIn('core/action/shutdown', server.handled_requests()[0])
