from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.zap_api.rules_parser import RulesParser
from test.unit.factories.zap_api import ascan, pscan


class TestRulesParser(TestCase):

    def setUp(self) -> None:
        self.config = MagicMock()
        self.rules_parser = RulesParser(self.config)

    def test_can_parse_passive_scanner_rule(self):
        result = pscan.scanners(pscan.scanner(name='passive scanner', plugin_id='10001', enabled='true'))
        rules = self.rules_parser.parse(result)

        self.assertEqual(1, len(rules))
        self.assertEqual('10001', str(rules[0].rule_id()))
        self.assertEqual(True, rules[0].is_enabled())
        self.assertEqual('passive scanner', rules[0].name())

    def test_can_parse_active_scanner_rule(self):
        result = ascan.scanners(ascan.scanner(name='active scanner', plugin_id='10002', enabled='false'))
        rules = self.rules_parser.parse(result)

        self.assertEqual(1, len(rules))
        self.assertEqual('10002', str(rules[0].rule_id()))
        self.assertEqual(False, rules[0].is_enabled())
        self.assertEqual('active scanner', rules[0].name())

    def test_considers_filtered_out_passive_rule_disabled(self):
        self.config.exclude_rules = ['10001']
        result = pscan.scanners(pscan.scanner(name='passive scanner', plugin_id='10001', enabled='true'))
        rules = self.rules_parser.parse(result)

        self.assertEqual(1, len(rules))
        self.assertEqual('10001', str(rules[0].rule_id()))
        self.assertEqual(False, rules[0].is_enabled())

    def test_skips_malformed_content(self):
        # This actually happens on the test_full_api_scan
        result = pscan.scanners('d')
        rules = self.rules_parser.parse(result)

        self.assertEqual(0, len(rules))
