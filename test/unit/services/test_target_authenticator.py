from unittest import TestCase
from unittest.mock import DEFAULT, Mock, patch

from src.models import Target
from src.models.errors import TargetNotAccessibleError
from src.services import TargetAuthenticator


class TestTargetAuthenticator(TestCase):

    def setUp(self):
        self.config = Mock()
        self.proxy = 'http://proxy'
        self.target = Target('http://vulnerable.website')
        self.zap = Mock()

    def test_authenticate_logs_into_target(self):
        self.config.auth_verification_url = ''
        authenticator = TargetAuthenticator(self.target, self.zap, self.config, self.proxy)

        with patch('src.services.target_authenticator.ZapWebdriver') as mock_webdriver:
            authenticator.authenticate()

        mock_webdriver.assert_called_once_with(self.config)
        mock_webdriver.return_value.setup_webdriver.assert_called_once_with(self.proxy)
        mock_webdriver.return_value.login.assert_called_once_with(self.zap, self.target)
        mock_webdriver.return_value.cleanup.assert_called_once()

    def test_authenticate_tests_auth_if_asked(self):
        check = Mock()
        check.status_code.return_value = 200
        self.config.auth_verification_url = 'http://vulnerable.website/loggedin'
        authenticator = TargetAuthenticator(self.target, self.zap, self.config, self.proxy)

        with patch.multiple(
            'src.services.target_authenticator',
            TargetProbe=DEFAULT,
            ZapWebdriver=DEFAULT,
        ) as mock_deps:
            mock_probe = mock_deps['TargetProbe']
            mock_probe.return_value.send_ping.return_value = check

            authenticator.authenticate()

        mock_probe.assert_called_once_with(
            'http://vulnerable.website/loggedin',
            self.config,
            proxy='http://proxy',
            follow_redirects=False,
        )
        mock_probe.return_value.send_ping.assert_called_once()

    def test_authenticate_raises_error_if_auth_validation_fails(self):
        check = Mock()
        check.status_code.return_value = 302
        self.config.auth_verification_url = 'http://vulnerable.website/loggedin'
        authenticator = TargetAuthenticator(self.target, self.zap, self.config, self.proxy)

        with self.assertRaises(TargetNotAccessibleError) as error:
            with patch.multiple(
                'src.services.target_authenticator',
                TargetProbe=DEFAULT,
                ZapWebdriver=DEFAULT,
            ) as mock_deps:
                mock_probe = mock_deps['TargetProbe']
                mock_probe.return_value.send_ping.return_value = check

                authenticator.authenticate()

        self.assertEqual(
            str(error.exception),
            'DAST could not reach the auth verification URL http://vulnerable.website/loggedin. Exiting scan',
        )
