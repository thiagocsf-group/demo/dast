from unittest import TestCase, mock
from unittest.mock import mock_open, patch

from src.services import UrlListFileWriter


class TestUrlListFileWriter(TestCase):

    def test_writes_urls_to_file(self):
        urls_to_scan = ['https://example.com/1', 'https://example.com/2', 'https://example.com/3']
        path = '/path/to/write/file.txt'
        file_writer = UrlListFileWriter(urls_to_scan, path)

        with patch('src.services.url_list_file_writer.open', mock_open()) as m:
            file_writer.write()

        m.assert_called_once_with(path, 'w')
        handle = m()
        handle.write.assert_has_calls([
            mock.call('https://example.com/1\n'),
            mock.call('https://example.com/2\n'),
            mock.call('https://example.com/3\n'),
        ])
