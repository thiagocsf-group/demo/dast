from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, patch

from src.models.target import Target
from src.services.browserker import BrowserkerScan
from test.unit.mock_config import ToConfig


class TestBrowserkerScan(TestCase):

    def setUp(self) -> None:
        self.config = ToConfig(browserker_allowed_hosts=[])
        self.target = Target('http://example.com/path/1')

    def test_starts_browserker_process(self):
        with patch.multiple(
                'src.services.browserker.browserker_scan',
                json=DEFAULT,
                open=DEFAULT,
                os=DEFAULT,
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            mock_dependencies['System'].return_value = system
            system.run.return_value = process
            BrowserkerScan(self.config, self.target, MagicMock()).run()

        mock_dependencies['BrowserkerConfigurationFile'].return_value.write.assert_called_once()
        system.run.assert_called_once()
        process.poll.assert_called_once()

        self.assertIn('/browserker/analyzer', system.run.mock_calls[0][1][0])
        self.assertIn('run', system.run.mock_calls[0][1][0])

    def test_sets_global_exclude_urls(self):
        self.config.browserker_allowed_hosts = ['foo.com', 'bar.org', 'example.com']
        with patch.multiple(
                'src.services.browserker.browserker_scan',
                json=DEFAULT,
                open=DEFAULT,
                os=DEFAULT,
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:

            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            mock_dependencies['System'].return_value = system
            system.run.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            zaproxy = MagicMock()
            BrowserkerScan(self.config, self.target, zaproxy).run()

        zaproxy.set_global_exclude_urls.assert_called_once_with(
            '^(?:(?!https?:\\/\\/(foo.com|bar.org|example.com)).*).$')

    def test_returns_scanned_urls(self):
        browserker_response = {
            'audited_urls': [
                {'url': 'http://example.com/1'},
                {'url': 'http://example.com/2'},
                {'url': 'http://example.com/3'},
            ],
        }

        with patch.multiple(
                'src.services.browserker.browserker_scan',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT,
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:

            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            mock_dependencies['System'].return_value = system
            system.run.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=browserker_response)
            browserker = BrowserkerScan(self.config, self.target, MagicMock())
            browserker._set_zap_cookies = MagicMock()
            scanned_urls = browserker.run()

        self.assertEqual(['http://example.com/1', 'http://example.com/2', 'http://example.com/3'], scanned_urls)

    def test_sets_cookies(self):
        cookies_response = [
            {'name': 'name1', 'value': 'value1', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886752+09:00'},
            {'name': 'name2', 'value': 'value2', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886753+09:00'},
        ]

        with patch.multiple(
                'src.services.browserker.browserker_scan',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT,
                System=DEFAULT,
                BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:

            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            mock_dependencies['System'].return_value = system
            system.run.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=cookies_response)
            mock_zap = MagicMock()
            browserker = BrowserkerScan(self.config, self.target, mock_zap)
            browserker._get_browserker_urls = MagicMock()
            browserker.run()
            mock_zap.create_zap_httpsession.assert_called_with(cookies_response, self.target)
