from unittest import TestCase

from src.models import Rule


class TestRule(TestCase):

    def test_should_create_a_rule(self):
        rule = Rule('1001', True, 'rule')

        self.assertEqual('1001', str(rule.rule_id()))
        self.assertEqual(True, rule.is_enabled())
        self.assertEqual('rule', rule.name())
