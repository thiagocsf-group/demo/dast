import time
from contextlib import contextmanager
from typing import ContextManager

from .log_suppressed_http_request_handler import LoggedSuppressedHTTPRequestHandler
from .threaded_http_server import ThreadedHTTPServer


class HTTPServerBuilder:

    def __init__(self):
        self.responses = []
        self.https_certificate_path = None

    def https(self, certificate_path):
        self.https_certificate_path = certificate_path
        return self

    def respond(self, delay=0, status=200, headers={}, content=None):
        self.responses.append({
            'delay': delay,
            'status': status,
            'headers': headers,
            'content': content,
        })
        return self

    def next_response(self):
        if len(self.responses) == 0:
            raise RuntimeError("Handler: I don't know what to return! Have you added any responses?")

        # if there is only one, keep returning it
        if len(self.responses) == 1:
            return self.responses[0]

        return self.responses.pop(0)

    @contextmanager
    def build(builder) -> ContextManager[ThreadedHTTPServer]:
        requests = []

        class Handler(LoggedSuppressedHTTPRequestHandler):

            def do_GET(self):
                requests.append(self.path)
                response = builder.next_response()

                time.sleep(response['delay'])
                self.send_response(response['status'])
                [self.send_header(name, value) for (name, value) in response['headers'].items()]
                self.end_headers()

                if 'content' in response and response['content']:
                    self.wfile.write(response['content'].encode('utf-8'))
                    self.wfile.flush()

        server = ThreadedHTTPServer(Handler, builder.https_certificate_path, requests)
        server.start()

        try:
            yield server
        finally:
            server.stop()


def new():
    return HTTPServerBuilder()
