import unittest
from unittest.mock import MagicMock, Mock, call, patch

from src.zap_webdriver import ZapWebdriver


class TestZapWebdriver(unittest.TestCase):

    def setUp(self):
        self.webdriver = ZapWebdriver(MagicMock())
        self.webdriver.auth_username = 'user'
        self.webdriver.auth_password = 'pass'

    def test_setup_webdriver_uses_firefox_before_DAST_2(self):
        config = Mock()
        config.dast_major_version = 1
        webdriver = ZapWebdriver(config)

        with patch('src.zap_webdriver.webdriver') as mock_selenium:
            webdriver.setup_webdriver('https://my-proxy.test')

        mock_selenium.Proxy.assert_called_once()
        mock_selenium.Proxy.return_value.add_to_capabilities.assert_called_once_with({
            'httpProxy': 'https://my-proxy.test',
            'ftpProxy': 'https://my-proxy.test',
            'sslProxy': 'https://my-proxy.test',
            'noProxy': None,
            'proxyType': 'MANUAL',
            'class': 'org.openqa.selenium.Proxy',
            'autodetect': False,
        })
        mock_selenium.FirefoxOptions.assert_called_once()
        mock_selenium.FirefoxOptions.return_value.add_argument.assert_called_once_with('-headless')
        mock_selenium.FirefoxProfile.assert_called_once()
        self.assertTrue(mock_selenium.FirefoxProfile.return_value.accept_untrusted_certs)
        mock_selenium.FirefoxProfile.return_value.set_preference.assert_has_calls([
            call('browser.startup.homepage_override.mstone', 'ignore'),
            call('startup.homepage_welcome_url.additional', 'about:blank'),
            call('network.proxy.allow_hijacking_localhost', 'true'),
        ])
        mock_selenium.Firefox.assert_called_once_with(
            executable_path='/usr/bin/geckodriver',
            firefox_profile=mock_selenium.FirefoxProfile.return_value,
            proxy=mock_selenium.Proxy.return_value,
            options=mock_selenium.FirefoxOptions.return_value,
        )
        self.assertEqual(webdriver.driver, mock_selenium.Firefox.return_value)

    def test_setup_webdriver_uses_chrome_in_DAST_2(self):
        config = Mock()
        config.dast_major_version = 2
        webdriver = ZapWebdriver(config)

        with patch('src.zap_webdriver.webdriver') as mock_selenium:
            webdriver.setup_webdriver('https://my-proxy.test')

        mock_selenium.ChromeOptions.assert_called_once()
        mock_selenium.ChromeOptions.return_value.add_argument.assert_has_calls([
            call('--proxy-server=https://my-proxy.test'),
            call('--headless'),
        ])
        mock_selenium.Chrome.assert_called_once_with(
            executable_path='/usr/bin/chromedriver',
            options=mock_selenium.ChromeOptions.return_value,
        )
        self.assertEqual(webdriver.driver, mock_selenium.Chrome.return_value)

    def test_auto_login_searches_and_fills_in_user(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("(//input[(@type='text' and contains(@name,'ser'))"
                              " or @type='text'])[1]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_username)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_fills_in_pass(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//input[@type='password' or contains(@name,'ass')]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_password)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_clicks_submit(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//*[@type='submit' or @type='button']"),
                         call().click()]

        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_cleanup_is_silent_when_webdriver_is_not_used(self):
        self.webdriver.driver = None

        # no error should be thrown
        self.webdriver.cleanup()

    def test_cleanup_releases_resources(self):
        self.webdriver.driver = MagicMock()

        self.webdriver.cleanup()

        self.webdriver.driver.quit.assert_called_once()
