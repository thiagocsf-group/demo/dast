

def alert_instance(attack='http://nginx/attack',
                   evidence='<form action="/myform" method="POST">',
                   uri='http://nginx/',
                   param='X-XSS-Protection',
                   method='POST'):
    return {
        'attack': attack,
        'evidence': evidence,
        'uri': uri,
        'param': param,
        'method': method,
    }
