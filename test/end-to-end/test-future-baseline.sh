#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}
MAX_SCAN_DURATION_SECONDS=${MAX_SCAN_DURATION_SECONDS:-66}

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_future_baseline() {
  docker run --rm \
    -v "${PWD}":/output \
    -v "${PWD}/fixtures/scripts":/home/zap/custom-scripts \
    --network test \
    --env DAST_MASK_HTTP_HEADERS=Cache-Control,Pragma \
    --env DAST_SCRIPT_DIRS=/home/zap/custom-scripts \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_future_baseline.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_future_baseline.json

  diff -u <(./normalize_dast_report.py expect/test_future_baseline.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_future_baseline.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
