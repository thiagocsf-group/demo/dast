from io import StringIO
from os import linesep, path

from invoke import run


class Tests:

    def __init__(self, name: str, file_path: str, file='', test=''):
        self._name = name
        self._file_path = file_path
        self._file = file
        self._test = test

    def execute(self):
        if self._test:
            return self._run_single_test()

        if self._file:
            return self._run_test_file()

        self._run_all_tests()

    def _run_all_tests(self):
        run('python -m unittest discover '
            '--top-level-directory . '
            f'--start-directory {self._file_path}')

    def _run_test_file(self):
        # Tests can be run with the relative path to the file
        file_name = self._file.split('/')[-1]

        run('python -m unittest discover '
            '--top-level-directory . '
            f'--pattern {file_name} '
            f'--start-directory {self._file_path}')

    def _run_single_test(self) -> None:
        find_output = self._run_and_grab_output(f'find {self._file_path} -name "*.py" | xargs grep -l {self._test}')
        test_files = find_output.strip()

        if not test_files:
            print(f'{self._name} test {self._test} not found.')
            exit(1)

        tests = [self._full_path_to_test(test_file) for test_file in test_files.split(linesep)]
        run(f'python -m unittest {" ".join(tests)}')

    def _run_and_grab_output(self, command: str) -> str:
        out_stream = StringIO()
        run(command, out_stream=out_stream, warn=True)
        return out_stream.getvalue()

    def _full_path_to_test(self, test_file: str) -> str:
        class_output = self._run_and_grab_output(f'grep "class .*(.*TestCase)" {test_file.strip()}')
        package_name = test_file.replace(path.sep, '.').replace('.py', '')
        test_class = class_output.replace('class ', '').split('(', 1)[0]

        return f'{package_name}.{test_class}.{self._test}'
