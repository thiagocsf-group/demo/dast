from .host import Host


class AjaxSpiderServer:

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name ajax_spider_server '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/ajax-spider":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/ajax-spider/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-d nginx:1.17.6')

        print(f'Ajax spider server started at http://{self.host.name()}:{self.port}/food.html')
        return self

    def stop(self):
        self.run('docker rm --force ajax_spider_server >/dev/null 2>&1 || true')
        return self
