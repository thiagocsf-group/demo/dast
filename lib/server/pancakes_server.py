from .host import Host


class PancakesServer:

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name pancakes_server '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/pancakes":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/pancakes/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-d nginx:1.17.6-alpine')

        print(f'Pancakes server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force pancakes_server >/dev/null 2>&1 || true')
        return self
