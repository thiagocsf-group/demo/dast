from argparse import ArgumentTypeError

from six.moves.urllib.parse import urlparse


class ArgumentType:

    @classmethod
    def url(cls, value):
        try:
            result = urlparse(value)
            if all([result.scheme, result.netloc]) is not True:
                raise ArgumentTypeError(f'{value} is not a valid URL')
            else:
                return value
        except Exception:
            raise ArgumentTypeError(f'{value} is not a valid URL')
