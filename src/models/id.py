class ID:

    INVALID_INT_MESSAGE = 'ID must be an integer'

    def __init__(self, object_id: str):
        self._id = object_id

    def valid(self) -> bool:
        return bool(self._id) and self._id.isdigit()

    def __eq__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            return str(other) == str(self)

        return False

    def __int__(self) -> int:
        if self.valid():
            return int(self._id)

        raise ValueError(self.INVALID_INT_MESSAGE)

    def __json__(self) -> str:
        return str(self)

    def __str__(self) -> str:
        if self.valid():
            return self._id

        return ''
