import logging
from typing import Any, Callable

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class ZapWebdriver:

    SUBMIT_BUTTON_XPATH = ("//*[@type='submit' or @type='button']")

    def __init__(self, config):
        self.driver = None
        self.auth_auto = config.auth_auto
        self.auth_login_url = config.auth_url or ''
        self.auth_username = config.auth_username or ''
        self.auth_password = config.auth_password or ''
        self.auth_username_field_name = config.auth_username_field or ''
        self.auth_password_field_name = config.auth_password_field or ''
        self.auth_submit_field_name = config.auth_submit_field or ''
        self.auth_first_submit_field_name = config.auth_first_submit_field or ''
        self._dast_major_version = config.dast_major_version

    def setup_webdriver(self, proxy_address: str) -> None:
        logging.debug('Setup proxy for webdriver')
        logging.debug('PROXY: ' + proxy_address)
        logging.debug('Start webdriver')

        if self._dast_major_version < 2:
            self._start_firefox(proxy_address)
        else:
            self._start_chrome(proxy_address)

        self.driver.implicitly_wait(30)

    def login(self, zap, target):
        logging.debug('Authenticate using webdriver ' + self.auth_login_url)

        self.driver.get(self.auth_login_url)

        if self.auth_auto:
            self.auto_login(zap, target)
        else:
            self.normal_login(zap, target)

        logging.debug('Create an authenticated session')

        # Adding a session token is required to use set_session_token_value but the name does not seem to matter.
        zap.httpsessions.add_session_token(target, 'dast_session')

        # Create a new session using the aquired cookies from the authentication
        zap.httpsessions.create_empty_session(target, 'auth-session')

        # add all found cookies as session cookies
        for cookie in self.driver.get_cookies():
            zap.httpsessions.set_session_token_value(
                target, 'auth-session', cookie['name'], cookie['value'])
            logging.debug('Cookie found: ' + cookie['name'] + ' - Value: ' + cookie['value'])

        # Mark the session as active
        zap.httpsessions.set_active_session(target, 'auth-session')
        logging.debug('Active session: ' + zap.httpsessions.active_session(target))

    def auto_login(self, zap, target):
        logging.debug('Automatically finding login fields')

        if self.auth_username:
            # find username field
            userField = self.driver.find_element_by_xpath(
                "(//input[(@type='text' and contains(@name,'ser')) or @type='text'])[1]")
            userField.clear()
            userField.send_keys(self.auth_username)

        # find password field
        try:
            if self.auth_password:
                passField = self.driver.find_element_by_xpath(
                    "//input[@type='password' or contains(@name,'ass')]")
                self._fill_in_password(passField)

            sumbitField = self.driver.find_element_by_xpath(self.SUBMIT_BUTTON_XPATH)
            sumbitField.click()
        except NoSuchElementException:
            logging.debug('Did not find password field - auth in 2 steps')
            # login in two steps
            sumbitField = self.driver.find_element_by_xpath(self.SUBMIT_BUTTON_XPATH)
            sumbitField.click()
            if self.auth_password:
                passField = self.driver.find_element_by_xpath(
                    "//input[@type='password' or contains(@name,'ass')]")
                self._fill_in_password(passField)
            sumbitField = self.driver.find_element_by_xpath(self.SUBMIT_BUTTON_XPATH)
            sumbitField.click()

    def normal_login(self, zap, target):
        if self.auth_username_field_name:
            userField = self.find_element(self.auth_username_field_name, None)
            userField.clear()
            userField.send_keys(self.auth_username)

        if self.auth_first_submit_field_name:
            self.find_element(self.auth_first_submit_field_name, self.SUBMIT_BUTTON_XPATH).click()

        if self.auth_password_field_name:
            passwordField = self.find_element(self.auth_password_field_name, None)
            self._fill_in_password(passwordField)

        self.find_element(self.auth_submit_field_name, self.SUBMIT_BUTTON_XPATH).click()

    def find_element(self, name, xpath):
        if name is None and xpath is None:
            raise ValueError('Either name or xpath must be provided')

        # search element first by id, then by name, then by xpath
        if name is not None:
            try:
                return self.driver.find_element_by_id(name)
            except NoSuchElementException:
                try:
                    return self.driver.find_element_by_name(name)
                except NoSuchElementException:
                    if xpath is None:
                        raise

        return self.driver.find_element_by_xpath(xpath)

    def cleanup(self) -> None:
        self._execute_ignoring_error(lambda: self.driver.quit())

    def _start_firefox(self, proxy_address: str) -> None:
        proxy = webdriver.Proxy()
        proxy.add_to_capabilities({
            'httpProxy': proxy_address,
            'ftpProxy': proxy_address,
            'sslProxy': proxy_address,
            'noProxy': None,
            'proxyType': 'MANUAL',
            'class': 'org.openqa.selenium.Proxy',
            'autodetect': False,
        })

        options = webdriver.FirefoxOptions()
        options.add_argument('-headless')

        profile = webdriver.FirefoxProfile()
        profile.accept_untrusted_certs = True
        profile.set_preference('browser.startup.homepage_override.mstone', 'ignore')
        profile.set_preference('startup.homepage_welcome_url.additional', 'about:blank')
        profile.set_preference('network.proxy.allow_hijacking_localhost', 'true')

        self.driver = webdriver.Firefox(executable_path='/usr/bin/geckodriver',
                                        firefox_profile=profile, proxy=proxy, options=options)

    def _start_chrome(self, proxy_address: str) -> None:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument(f'--proxy-server={proxy_address}')
        chrome_options.add_argument('--headless')

        self.driver = webdriver.Chrome(
            executable_path='/usr/bin/chromedriver',
            options=chrome_options,
        )

    def _fill_in_password(self, passField: Any) -> None:
        logging.debug('Disabling logging to fill in password')
        logging.disable()

        try:
            passField.clear()
            passField.send_keys(self.auth_password)
        finally:
            logging.disable(logging.NOTSET)
            logging.debug('Re-enabling logging')

    def _execute_ignoring_error(self, func: Callable[[], None]) -> None:
        try:
            func()
        except:                                  # noqa: E722 as this intentionally catches all errors.
            # intentionally suppress error
            pass
