from typing import Optional, Tuple

from requests import Response
from requests.exceptions import RequestException

from src import Configuration


class TargetAvailabilityCheck:

    def __init__(
        self, is_available: bool, config: Configuration,
        response: Optional[Response] = None,
        unavailable_reason: Optional[RequestException] = None,
    ):
        self._is_available = is_available
        self._config = config
        self._response = response
        self._unavailable_reason = unavailable_reason

    def status_code(self) -> Optional[int]:
        if self._response is not None:
            return self._response.status_code

        return None

    def is_available(self) -> bool:
        return self._is_available

    def unavailable_reason(self) -> Optional[RequestException]:
        return self._unavailable_reason

    def is_safe_to_scan(self) -> Tuple[bool, str]:
        if self._config.dast_major_version >= 2 or self._config.is_api_scan or not self._config.full_scan:
            return True, ''

        if not self._response:
            return False, 'Attempting to full scan, but the site is unavailable. Permissions cannot be verified.'

        permission = self._response.headers.get('gitlab-dast-permission')

        if permission != 'allow' and self._config.full_scan_domain_validation_required:
            return False, 'The application has not explicitly allowed DAST scans'

        if permission == 'deny':
            return False, 'The application has explicitly denied DAST scans'

        return True, ''
