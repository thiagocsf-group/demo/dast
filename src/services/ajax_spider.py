import logging
import time

from src.models import Target
from src.zap_gateway import ZAProxy


class AJAXSpider:

    SPIDER_POLL_DELAY_SECONDS = 5

    def __init__(self, zap: ZAProxy, target: Target, max_mins_to_run: int):
        self._zap = zap
        self._target = target
        self._max_mins_to_run = max_mins_to_run

    def run(self) -> None:
        logging.info(f'Ajax Spider starting with target: {self._target}')

        if self._max_mins_to_run:
            self._zap.set_ajax_spider_maximum_run_time(str(self._max_mins_to_run))

        self._zap.run_ajax_spider(self._target)

        time.sleep(self.SPIDER_POLL_DELAY_SECONDS)

        while self._zap.ajax_spider_status() == 'running':
            logging.info(f'Ajax Spider running: found {self._zap.ajax_spider_results_count()} URLs so far')

            time.sleep(self.SPIDER_POLL_DELAY_SECONDS)

        logging.info(f'Ajax Spider complete: found {self._zap.ajax_spider_results_count()} URLs')
