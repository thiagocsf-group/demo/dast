import logging
from typing import Any, Dict, List, Tuple, cast


Rule = Dict[str, List[str]]
HostProcess = Tuple[str, Dict[str, List[Rule]]]


class ActiveScanLogger:

    def __init__(self, scan_data: List[Any]):
        self._scan_data = scan_data

    def log(self) -> None:
        self._log_host_processes()

    def _log_host_processes(self) -> None:
        for host, rule_data in self._host_processes():
            rules = rule_data['HostProcess']

            for rule in rules:
                self._log_rule(rule, host)

    def _log_rule(self, rule_data: Rule, host: str) -> None:
        rule = rule_data['Plugin']

        rule_name, \
            rule_id, \
            _rule_release_status, \
            rule_status, \
            rule_scan_duration_ms, \
            request_count, \
            alert_count = rule

        logging.info(
            f'Active scan rule {rule_name} [{rule_id}] [{rule_status}] ran on {host}, '
            f'took {rule_scan_duration_ms}ms, '
            f'made {request_count} request(s), '
            f'and found {alert_count} alert(s).',
        )

    def _host_processes(self) -> List[HostProcess]:
        return [
            cast(
                HostProcess,
                tuple(self._scan_data[i:i + 2]),
            ) for i in range(0, len(self._scan_data), 2)
        ]
